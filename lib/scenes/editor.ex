defmodule Nota.Scene.Editor do
  @moduledoc """
  Editor scene.
  """

  use Scenic.Scene
  alias Scenic.Graph

  alias Nota.Component.TreeView

  import Scenic.Primitives, only: [{:text, 3}]

  @notes """
    \"Editor\" shows the main editor window.
  """

  @graph Graph.build(font: :roboto, font_size: 24)
         |> text("Editor", translate: {15, 20})
         |> TreeView.add_to_graph( __MODULE__ )

  def init(vp, _opts) do
    {:ok, @graph, push: @graph}
  end
end
