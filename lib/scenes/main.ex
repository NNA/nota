defmodule Nota.Scene.Main do
  @moduledoc """
  This is the main scene.
  """

  use Scenic.Scene

  alias Scenic.Graph
  alias Scenic.ViewPort

  @finish_delay_ms 1

  def init(first_scene, opts) do
    viewport = opts[:viewport]

    state = %{
      viewport: viewport,
      first_scene: first_scene
    }

    Process.send_after(self(), :finish, @finish_delay_ms)

    {:ok, state}
  end

  def handle_info(:finish, state) do
    go_to_first_scene(state)
    {:noreply, state}
  end

  defp go_to_first_scene(%{viewport: vp, first_scene: first_scene}) do
    ViewPort.set_root(vp, {first_scene, nil})
  end
end
