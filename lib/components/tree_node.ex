defmodule Nota.Component.TreeNode do
  use Scenic.Component

  alias Scenic.ViewPort
  alias Scenic.Graph

  import Scenic.Primitives, only: [{:text, 3}, {:rect, 3}, {:path, 3}, {:triangle, 3}]

  @width 160

  @height 110
  @font_size 20
  @indent 30

  #--------------------------------------------------------
  def verify( title ) when is_bitstring(title), do: {:ok, title}
  def verify( _ ), do: :invalid_data

  #----------------------------------------------------------------------------
  def init( title, opts ) do
    styles = opts[:styles] || %{}

    # Get the viewport width
    {:ok, %ViewPort.Status{size: {vp_width,vp_height}}} = opts[:viewport]
    |> ViewPort.info()

    IO.puts "vp_height #{vp_height}"
    IO.puts "vp_height/480 #{vp_height/480}"

    graph = Graph.build( styles: styles, font_size: 14 )
    |> triangle( {{0, 10}, {10, 20}, {0, 30}}, fill: :grey, translate: {5, -12}, scale: 0.5, id: :file_expand)
    |> path([
        :begin,
        {:move_to, 403.9, 97.85},
        {:line_to, 289.9, 97.85},
        {:bezier_to, 262.2, 97.85, 239.6, 75.35, 239.6, 47.55},
        {:bezier_to, 239.6, 40.05, 233.6, 34.05, 226.01, 34.05},
        {:line_to, 77.3, 34.05},
        {:bezier_to, 34.7, 34.05, 0, 68.75, 0, 111.35},
        {:line_to, 0, 369.85},
        {:bezier_to, 0, 412.45, 34.7, 447.15, 77.3, 447.15},
        {:line_to, 403.90, 447.15},
        {:bezier_to, 446.50, 447.15, 481.20, 412.45, 481.20, 369.85},
        {:line_to, 481.20, 175.05},
        {:bezier_to, 481.2, 132.45, 446.5, 97.85, 403.9, 97.85},
        :close_path
      ],
      stroke: {60, :white}, scale: 0.03, translate: {20, 0}, id: :file_picto)
    |> text( title, translate: {40, 12.5}, id: :file_title )

    {:ok, %{graph: graph, viewport: opts[:viewport]}, push: graph }
  end

end
