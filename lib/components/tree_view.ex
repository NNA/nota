defmodule Nota.Component.TreeView do
  use Scenic.Component

  alias Scenic.ViewPort
  alias Scenic.Graph

  import Scenic.Primitives, only: [{:text, 3}, {:rect, 3}]
  import Scenic.Components, only: [{:dropdown, 3}]

  alias Nota.Component.TreeNode

  # import IEx

  @width 160

  #--------------------------------------------------------
  def verify( scene ) when is_atom(scene), do: {:ok, scene}
  def verify( {scene,_} = data ) when is_atom(scene), do: {:ok, data}
  def verify( _ ), do: :invalid_data

  #----------------------------------------------------------------------------
  def init( _current_scene, opts ) do
    styles = opts[:styles] || %{}

    # Get the viewport width
    {:ok, %ViewPort.Status{size: {_,height}}} = opts[:viewport]
    |> ViewPort.info()

    graph = Graph.build( styles: styles, font_size: 14 )
    |> rect({@width, height}, fill: {48,48,48})
    # |> text("File:", translate: {14, 35}, align: :right)
    # |> dropdown({[
    #     {"File 1", 'path_to_file_1'},
    #     {"File 2", 'path_to_file_2'},
    #   ], 'path_to_file_1'}, id: :tree_view)
    # |> digital_clock(text_align: :right, translate: {width - 20, 35})
    |> TreeNode.add_to_graph( "Folder 1" )

    {:ok, %{graph: graph, viewport: opts[:viewport]}, push: graph }
  end

  #----------------------------------------------------------------------------
  # def filter_event( {:value_changed, :tree_view, scene}, _, %{viewport: vp} = state ) when is_atom(scene) do
  #   IO.puts "value_chaged"
  #    # ViewPort.set_root( vp, {scene, nil} )
  #   {:stop, state }
  # end

  #----------------------------------------------------------------------------
  def filter_event( {:value_changed, :tree_view, _scene}, _, %{viewport: vp} = state ) do
     # ViewPort.set_root( vp, scene )
     IO.puts "value_chaged"
    {:stop, state }
  end

end
